import { Component } from '@angular/core';
import { Constants } from './constants/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  
  getAppFirstName(){
    return Constants.AppFirstName;
  }

  getAppSecondName(){
    return Constants.AppSecondName;
  }

  getColor(){
    return "black";
  }
}